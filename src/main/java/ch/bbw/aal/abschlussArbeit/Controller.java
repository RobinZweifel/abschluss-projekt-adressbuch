package ch.bbw.aal.abschlussArbeit;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

/**
 * Controller
 *
 * @author Aaron Läuchli
 * @version 15.12.2020
 */

public class Controller {
    Model model;

    public void initialize(Model model) {
        this.model = model;

    }

    public void newPerson(ActionEvent actionEvent) {
    }

    public void editPerson(ActionEvent actionEvent) {
    }

    public void deletePerson(ActionEvent actionEvent) {
    }
}
