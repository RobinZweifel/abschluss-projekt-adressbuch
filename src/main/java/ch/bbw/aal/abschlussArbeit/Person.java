package ch.bbw.aal.abschlussArbeit;

import java.util.Date;

public class Person {
    public String vorname;
    public String nachname;
    public String strasse;
    public String plz;
    public String stadt;
    public String gebDate;


    public Person(String vorname, String nachname, String strasse, String plz, String stadt, String gebDate) {
        this.vorname = vorname;
        this.nachname = nachname;
        this.strasse = strasse;
        this.plz = plz;
        this.stadt = stadt;
        this.gebDate = gebDate;
    }

    @Override
    public String toString() {
        return this.vorname + " " + this.nachname;
    }

    public String getVorname() {
        return vorname;
    }

    public String getNachname() {
        return nachname;
    }

    public String getStrasse() {
        return strasse;
    }

    public String getPlz() {
        return plz;
    }

    public String getStadt() {
        return stadt;
    }

    public String getGebDate() {
        return gebDate;
    }
}
