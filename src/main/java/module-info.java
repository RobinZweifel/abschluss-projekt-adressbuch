module ch.bbw.pr.helloworldfx {
	requires transitive javafx.base;
    requires transitive javafx.controls;
    requires transitive javafx.graphics;
	requires transitive javafx.fxml;

    opens ch.bbw.aal.abschlussArbeit to javafx.fxml;
    exports ch.bbw.aal.abschlussArbeit;
}